<?php

namespace Tests\Unit\GraphQL\TestPanel\Types;

use App\GraphQL\TestPanel\Types\CnpjType;
use PHPUnit\Framework\TestCase;
use Tests\CreatesApplication;

class CnpjTypeTest extends TestCase
{
    use CreatesApplication;

    const CNPJ_INVALID_FORMATED = '13.010.710/0001-11';
    const CNPJ_VALID_FORMATED = '13.010.710/0001-44';
    const CNPJ_VALID_UNDFORMATED = '13010710000144';

    protected function setUp(): void
    {
        parent::setUp();
        $this->createApplication();
    }

    public function testSerializeCnpj()
    {
        $cnpjType = new CnpjType();
        $this->assertEquals(
            self::CNPJ_VALID_FORMATED,
            $cnpjType->serialize(self::CNPJ_VALID_UNDFORMATED)
        );
    }

    public function testSerializeNull()
    {
        $cnpjType = new CnpjType();
        $this->assertEquals(
            null,
            $cnpjType->serialize(null)
        );
    }

    public function testParseCnpj()
    {
        $cnpjType = new CnpjType();
        $this->assertEquals(
            self::CNPJ_VALID_UNDFORMATED,
            $cnpjType->parseValue(self::CNPJ_VALID_FORMATED)
        );
    }

    public function testParseCnpjNullableValue()
    {
        $cnpjType = new CnpjType();
        $this->assertEquals(null, $cnpjType->parseValue(null));
    }

    public function cnpj()
    {
        return [
            ['11.111.111/1111-11'],
            ['11.111.111/0001-11'],
            ['11111111111111'],
            ['13.010.710/0001'],
            ['xx.xxx.xxx/xxxx-xx'],
            ['xxxxxxxxxxxxxx'],
            ['753.652.150-11'],
            [self::CNPJ_INVALID_FORMATED],
        ];
    }

    /**
     * @dataProvider cnpj
     */
    public function testParseCnpjSeguenceInvalid($cnpj)
    {
        $this->expectException(\Exception::class);
        $dataType = new CnpjType();
        $dataType->parseValue($cnpj);
    }
}
