<?php

namespace Tests\Unit\GraphQL\TestPanel\Types;

use App\GraphQL\TestPanel\Types\CpfType;
use PHPUnit\Framework\TestCase;
use Tests\CreatesApplication;

class CpfTypeTest extends TestCase
{
    use CreatesApplication;

    const CPF_INVALID_FORMATED = '753.652.150-11';
    const CPF_VALID_FORMATED = '753.652.150-28';
    const CPF_VALID_UNDFORMATED = '75365215028';

    protected function setUp(): void
    {
        parent::setUp();
        $this->createApplication();
    }

    public function testSerializeCpf()
    {
        $cpfType = new CpfType();
        $this->assertEquals(
            $cpfType->serialize(self::CPF_VALID_UNDFORMATED),
            self::CPF_VALID_FORMATED
        );
    }

    public function testSerializeNull()
    {
        $cpfType = new CpfType();
        $this->assertEquals(
            null,
            $cpfType->serialize(null)
        );
    }

    public function testParseCpf()
    {
        $cpfType = new CpfType();
        $this->assertEquals(
            self::CPF_VALID_UNDFORMATED,
            $cpfType->parseValue(self::CPF_VALID_FORMATED)
        );
    }

    public function testParseCpfNullableValue()
    {
        $cpfType = new CpfType();
        $this->assertEquals(null, $cpfType->parseValue(null));
    }

    public function cpf()
    {
        return [
            ['111.111.111-11'],
            ['11111111111'],
            ['753.652.150'],
            ['xxx.xxx.xxx-xx'],
            ['xxxxxxxxxxx'],
            ['87.786.506/0001-56'],
            [self::CPF_INVALID_FORMATED],
        ];
    }

    /**
     * @dataProvider cpf
     */
    public function testParseCpfSeguenceInvalid($cpf)
    {
        $this->expectException(\Exception::class);
        $dataType = new CpfType();
        $dataType->parseValue($cpf);
    }
}
