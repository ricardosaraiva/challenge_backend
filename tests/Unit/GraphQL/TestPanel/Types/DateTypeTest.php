<?php

namespace Tests\Unit\GraphQL\TestPanel\Types;

use App\GraphQL\TestPanel\Types\DateType;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class DateTypeTest extends TestCase
{
    public function testSerializeDate()
    {
        $dataType = new DateType();
        $this->assertEquals(
            '01/01/2020',
            $dataType->serialize(new \DateTime('2020-01-01'))
        );
    }

    public function testSerializeNull()
    {
        $dataType = new DateType();
        $this->assertEquals(
            null,
            $dataType->serialize(null)
        );
    }

    public function testParseDate()
    {
        $dataType = new DateType();
        $parse = $dataType->parseValue('15/01/2020');
        $this->assertInstanceOf(Carbon::class, $parse);
        $this->assertEquals('2020-01-15', $parse->format('Y-m-d'));
    }

    public function testParseDateNullableValue()
    {
        $dataType = new DateType();
        $this->assertEquals(null, $dataType->parseValue(null));
    }

    public function testParseDateInvalidValue()
    {
        $this->expectException(\Exception::class);
        $dataType = new DateType();
        $dataType->parseValue('30/02/2020');
    }
}
