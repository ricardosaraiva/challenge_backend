<?php

namespace Tests\Unit\GraphQL\TestPanel\Types;

use App\GraphQL\TestPanel\Types\RoleType;
use App\User;
use PHPUnit\Framework\TestCase;
use Tests\CreatesApplication;

class RoleTest extends TestCase
{
    use CreatesApplication;

    public function testSerializeRole()
    {
        $roleType = new RoleType();
        $this->assertEquals(
            User::ROLE_SHIPPER,
            $roleType->serialize(User::ROLE_SHIPPER)
        );
    }

    public function testParseRole()
    {
        $roleType = new RoleType();
        $this->assertEquals(
            User::ROLE_ADMIN,
            $roleType->parseValue(User::ROLE_ADMIN)
        );
    }

    public function testParseInvalidRole()
    {
        $this->expectException(\Exception::class);
        $dataType = new RoleType();
        $dataType->parseValue('invalid');
    }
}
