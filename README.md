# Instruções

### Para fazer o setup do projeto
- `./cli/initial-setup`

### Para rodar os teste unitários
- `./cli/phpunit`

# Testar GraphQL
Você poderá testar suas _queries_ e _mutations_ através da página em `http://localhost:8010/graphql/test-panel`.


# Tarefas implementadas

### Tarefa 1:
Insira na tabela users os campos:
 - cpf: varchar(11)
 - birth_dt: date

Atualize a query fetchUsers para retornar o CPF formatado e birth_dt no formato d/m/Y.

### Tarefa 2:
Adicione um sistema de papéis (roles) aos usuários, podendo assumir os pápeis de:
 - "SHIPPER" (Embarcador, quem contrata o frete);
 - "CARRIER" (Transportadora);
 - "ADMIN".

Atualize a query fetchUsers para retornar o papel do usuário.

### Tarefa 3:
Os usuários do tipo CARRIER deverão ter os seguintes dados adicionais:
 - CNPJ;
 - Valor de transporte por kg.
Atualize a query fetchUsers para retornar um subarray com os novos dados quando o usuário possuir esse papel. O CNPJ deve está formatado.
