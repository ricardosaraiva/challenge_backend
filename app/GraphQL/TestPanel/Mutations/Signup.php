<?php

namespace App\GraphQL\TestPanel\Mutations;

use App\GraphQL\Mutation;
use App\GraphQL\TestPanel\ObjectTypes\Carrier;
use App\GraphQL\TestPanel\ObjectTypes\ObjectTypes;
use App\GraphQL\TestPanel\Types\Types;
use App\User;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use Illuminate\Support\Facades\Hash;

class Signup extends Mutation
{
    /**
     * @return array
     */
    protected function args(): array
    {
        return [
            'name' => Type::nonNull(Types::string()),
            'email' => Type::nonNull(Types::string()),
            'password' => Type::nonNull(Types::string()),
            'birth_dt' => Type::nonNull(Types::date()),
            'cpf' => Type::nonNull(Types::cpf()),
            'role' => Types::role(),
            'carrier' =>  Carrier::mutation()
        ];
    }

    /**
     * @return Type
     */
    protected function typeResult(): Type
    {
        return Type::boolean();
    }

    /**
     * @param $root
     * @param $args
     * @return bool
     */
    protected function resolve($root, $args): bool
    {
        $data = [
            'name' => $args['name'],
            'email' => strtolower($args['email']),
            'cpf' => $args['cpf'],
            'birth_dt' => $args['birth_dt'],
            'password' => Hash::make($args['password']),
            'role' => $args['role'],
        ];

        if($args['role'] === User::ROLE_CARRIER && isset($args['carrier'])) {
            $data = array_merge($data, [
                'cnpj' => $args['carrier']['cnpj'],
                'transport_price' => $args['carrier']['transport_price'],
            ]);
        }

        User::create($data);

        return true;
    }
}
