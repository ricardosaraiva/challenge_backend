<?php

namespace App\GraphQL\TestPanel\Types;

use GraphQL\Type\Definition\Type;

class Types
{
    private static $date;
    private static $cpf;
    private static $cnpj;
    private static $role;

    public static function int()
    {
        return Type::int();
    }

    public static function string()
    {
        return Type::string();
    }

    public static function float()
    {
        return Type::float();
    }

    public static function date()
    {
        return self::$date ?: (self::$date = new DateType());
    }

    public static function cpf()
    {
        return self::$cpf ?: (self::$cpf = new CpfType());
    }

    public static function cnpj()
    {
        return self::$cnpj ?: (self::$cnpj = new CnpjType());
    }

    public static function role()
    {
        return self::$role ?: (self::$role = new RoleType());
    }
}
