<?php

namespace App\GraphQL\TestPanel\Types;

use Carbon\Carbon;
use Exception;
use GraphQL\Error\Error;
use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use Illuminate\Support\Facades\Validator;

class CpfType extends ScalarType
{
    public $name = 'CPF';
    public $description = 'Document CPF';

    private function cpfToNumber($cpf) {
        return preg_replace('/[^0-9]/', '', $cpf);
    }

    private function cpfFormatted($cpf) {
        return preg_replace('/(\d{3})(\d{3})(\d{3})(\d{2})/', '$1.$2.$3-$4', $cpf);
    }

    private function validate($value) {
        $validator = Validator::make(['cpf' => $value], ['cpf' => 'cpf']);

        if ($validator->fails() || strlen($value) !== 11) {
            throw new Exception('Invalid CPF');
        }
    }

    /**
     * @param string $value
     * @return string
     */
    public function serialize($value)
    {
        return !is_null($value) ? $this->cpfFormatted($value) : null;
    }

    /**
     * @param string $value
     * @throws \Exception
     */
    public function parseValue($value): ?string
    {
        if(is_null($value)) {
            return null;
        }

        $cpf = $this->cpfToNumber($value);
        $this->validate($cpf);
        return $cpf;
    }

    /**
     * @throws Error
     */
    public function parseLiteral(Node $valueNode, ?array $variables = null)
    {
        if ($valueNode instanceof StringValueNode) {
            return $this->parseValue($valueNode->value);
        }

        return null;
    }
}
