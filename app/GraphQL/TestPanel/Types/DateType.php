<?php

namespace App\GraphQL\TestPanel\Types;

use Carbon\Carbon;
use Exception;
use GraphQL\Error\Error;
use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;

class DateType extends ScalarType
{
    public $name = 'Date';
    public $description = 'Date in the format: dd/mm/yyyy';

    /**
     * @param string $value
     * @return string
     */
    public function serialize($value)
    {
        return !is_null($value) ? Carbon::parse($value)->format('d/m/Y') : null;
    }

    /**
     * @param string $value
     * @throws \Exception
     */
    public function parseValue($value): ?Carbon
    {

        if(is_null($value)) {
            return null;
        }

        $splitDate = explode('/', $value);

        if(!checkdate($splitDate[1], $splitDate[0], $splitDate[2])) {
            throw new \Exception(
                'Inválid date, expected format d/m/Y example: 01/01/2020'
            );
        }

        return Carbon::createFromFormat('d/m/Y', $value);
    }

    /**
     * @throws Error
     */
    public function parseLiteral(Node $valueNode, ?array $variables = null)
    {
        if ($valueNode instanceof StringValueNode) {
            return $this->parseValue($valueNode->value);
        }

        return null;
    }
}
