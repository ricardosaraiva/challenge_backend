<?php

namespace App\GraphQL\TestPanel\Types;

use Carbon\Carbon;
use Exception;
use GraphQL\Error\Error;
use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use Illuminate\Support\Facades\Validator;

class CnpjType extends ScalarType
{
    public $name = 'CNPJ';
    public $description = 'Document CNPJ';

    private function cnpjToNumber($cnpj) {
        return preg_replace('/[^0-9]/', '', $cnpj);
    }

    private function cnpjFormatted($cnpj) {
        return preg_replace('/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/', '$1.$2.$3/$4-$5', $cnpj);
    }

    private function validate($value) {
        $validator = Validator::make(['cnpj' => $value], ['cnpj' => 'cnpj']);

        if ($validator->fails() || strlen($value) !== 14) {
            throw new Exception('Invalid CNPJ');
        }
    }

    /**
     * @param string $value
     * @return string
     */
    public function serialize($value)
    {
        return !is_null($value) ? $this->cnpjFormatted($value) : null;
    }

    /**
     * @param string $value
     * @throws \Exception
     */
    public function parseValue($value): ?string
    {
        if(is_null($value)) {
            return null;
        }

        $cnpj = $this->cnpjToNumber($value);
        $this->validate($cnpj);
        return $cnpj;
    }

    /**
     * @throws Error
     */
    public function parseLiteral(Node $valueNode, ?array $variables = null)
    {
        if ($valueNode instanceof StringValueNode) {
            return $this->parseValue($valueNode->value);
        }

        return null;
    }
}
