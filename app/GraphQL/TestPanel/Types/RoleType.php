<?php

namespace App\GraphQL\TestPanel\Types;

use App\User;
use Carbon\Carbon;
use Exception;
use GraphQL\Error\Error;
use GraphQL\Language\AST\Node;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use Illuminate\Support\Facades\Validator;

class RoleType extends ScalarType
{
    public $name = 'Role';

    public $description;

    public function __construct(array $configs = [])
    {
        parent::__construct($configs);
        $this->description = sprintf(
            'role of users, valids roles: %s',
            implode(', ', User::ROLES)
        );
    }

    private function validate($value) {
        if (!in_array($value, User::ROLES)) {
            throw new Exception('Invalid role');
        }
    }

    /**
     * @param string $value
     * @return string
     */
    public function serialize($value)
    {
        return $value;
    }

    /**
     * @param string $value
     * @throws \Exception
     */
    public function parseValue($value): ?string
    {
        $this->validate($value);
        return $value;
    }

    /**
     * @throws Error
     */
    public function parseLiteral(Node $valueNode, ?array $variables = null)
    {
        if ($valueNode instanceof StringValueNode) {
            return $this->parseValue($valueNode->value);
        }

        return null;
    }
}
