<?php

namespace App\GraphQL\TestPanel\ObjectTypes;

use App\GraphQL\TestPanel\Types\Types;
use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class Carrier implements IObjectType
{
    /**
     * @var ObjectType
     */
    private static $fetch;

    /**
     * @var InputObjectType
     */
    private static $mutation;

    public static function fetch(): ObjectType
    {
        return self::$fetch ?: (self::$fetch =
            new ObjectType([
                'name' => 'CarrierFetch',
                'fields' => [
                    'cnpj' => Types::cnpj(),
                    'transport_price' => Type::float(),
                ],
            ])
        );
    }

    public static function mutation(): InputObjectType
    {
        return self::$mutation ?: (self::$mutation =
            new InputObjectType([
                'name' => 'CarrierMutation',
                'fields' => [
                    'cnpj' => Types::cnpj(),
                    'transport_price' => Type::float(),
                ]
            ])
        );
    }
}
