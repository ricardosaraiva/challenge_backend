<?php

namespace App\GraphQL\TestPanel\ObjectTypes;

use GraphQL\Type\Definition\InputObjectType;
use GraphQL\Type\Definition\ObjectType;

interface IObjectType
{
    public static function fetch(): ObjectType;
    public static function mutation(): InputObjectType;
}
