<?php

namespace App\GraphQL\TestPanel\Queries;

use App\GraphQL\QueryPaginated;
use App\GraphQL\TestPanel\ObjectTypes\Carrier;
use App\GraphQL\TestPanel\Types\Types;
use App\User;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class FetchUsers extends QueryPaginated
{
    /**
     * @return array
     */
    protected function args(): array
    {
        return [
            'like' => Type::string(),
        ];
    }

    /**
     * @return Type
     */
    protected function typeResult(): Type
    {
        return new ObjectType([
            'name' => 'FetchUsersResult',
            'fields' => [
                'id' => Types::int(),
                'name' => Types::string(),
                'email' => Types::string(),
                'cpf' => Types::cpf(),
                'role' => Types::role(),
                'birth_dt' => Types::date(),
                'carrier' => [
                    'type' => Carrier::fetch(),
                    'description' => 'Data of the user type carrier',
                    'resolve' => function($user) {
                        return $user['role'] === User::ROLE_CARRIER ? $user : null;
                    }
                ]
            ],
        ]);
    }

    /**
     * @param $root
     * @param $args
     * @return array
     */
    protected function resolve($root, $args): array
    {
        $builder = User::query();

        /*
         * Fetch paginate
         */
        $limit = isset($args['limit']) ? $args['limit'] : 15;
        $users = $builder->paginate($limit);

        return $users->toArray();
    }
}
